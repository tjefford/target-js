 /****************************************  // // Written by Tyler Jefford
 // Website http://divisionoverlay.net
 // Version 4.0.0
 // Purpose Detect browsers and trigger
 //         an action.
 //
 ***************************************/
 
 (function( $ ){
 
   $.fn.target = function( options,callback ) {  
 
 	var settings = $.extend( {
 	  'browser' : '',
 	  'ver' : '',
 	  'msg' : '',
 	  'action' : 'banner', 
 	  'url' : '',
 	  'cssUrl' : ''
 	}, options);
 
 	return this.each(function() {        
 
 	  var op = options;   
 	  
 	  function custom() {
 	    settings.action.call(this);
     }
 
 	  function doAction(){
 		  //Banner
 		  if(op.action == "banner" && op.msg){
 			$('body').prepend('<div id="target_message">'+ op.msg +'<a href="#" class="target_close">X</a></div>');
 		  }
 
 		  //Relocate
 		  if(op.action == "relocate" && op.url){
 			window.open (op.url,'_self',true)
 		  }
 
 		  //Alert
 		  if(op.action == "alert" && op.msg){
 			alert(op.msg);
 		  }
 
 		  //Styles
 		  if(op.action == "style" && op.cssUrl){
 		  
 		  	var place = document.getElementsByTagName("head")[0];
 		  	var newCss = document.createElement('link');
 		  	newCss.type = 'text/css';
 		  	newCss.rel = 'stylesheet';
 		  	newCss.href = op.cssUrl;
 		  	newCss.media = 'screen';
 		  	place.appendChild(newCss);
 		   
 		}
 	  }
 
 	  // ================= Begin Browser Detection ==================== //
 
 	  //Detect webkit based browsers
 	  if(op.browser == "safari" || op.browser == "chrome" || op.browser == "webkit"){
 
 			var webkit = $.browser.webkit;
 
 			if(webkit){
 				doAction();
 			}
 
 		}
 
 		//Detect Mozilla based Browsers
 		if(op.browser == "firefox"  || op.browser == "moz" || op.browser == "mozilla"){
 
 			var moz = $.browser.mozilla;
 
 			if(moz){
 				doAction();
 			}
 
 		}
 
 		//Detect Opera Browser
 		if(op.browser == "opera"){
 
 			var opera = $.browser.opera;
 
 			if(opera){
 				doAction();
 			}
 
 		}
 
 		//Detect IE browsers
 		if(op.browser == "ie" && op.ver){
 
 			//Sets the .0 on the version number
 			var ver = op.ver+".0";
 
 			//This will get the version number
 			var clientVersion = $.browser.version;
 
 			//Target Internet Explorer 6 - 10
 			var ie = $.browser.msie;
 
 			if(ie && clientVersion == ver){
 				doAction();
 			}
 
 
 		}
 
 	  // ================= End Browser Detection ==================== //
 
 
 
 	  // ================= Begin Banner Styles ==================== //
 
 		//Close banner
 		$(".target_close").click(function(e){
 			e.preventDefault();
 			$("#target_message").slideUp("slow");
 		});
 		$("#target_message").hide();
 		$("#target_message").delay("1000").slideDown("slow");
 
 		//Target-Message Styles
 		$("#target_message").css({'width':'100%','height':'33px','position':'relative','z-index':'9999','background-color':'#faf4c7','text-align':'center','padding-top':'8px','color':'#333'});
 		$(".target_close").css({'width':'20px','float':'right','margin-right':'100px','color':'#333','text-decoration':'none'});
 
 	// ================= End Banner Styles ==================== //
 
 	});
 
   };
 })( jQuery );